<?php

namespace ThibaudDauce\Mikrotik\Connections;

use ThibaudDauce\Mikrotik\Command;

interface Connection
{
    public function exec(Command $command);
}
