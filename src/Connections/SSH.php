<?php

namespace ThibaudDauce\Mikrotik\Connections;

use ErrorException;
use ThibaudDauce\Mikrotik\Command;

class SSH implements Connection
{
    public $ssh;

    public function __construct($host, $port, $login, $password)
    {
        try {
            $connection = ssh2_connect($host, $port);
        } catch (ErrorException $e) {
            // try again in case of random exception.
            $connection = ssh2_connect($host, $port);
        }

        ssh2_auth_password($connection, $login, $password);

        $this->ssh = $connection;
    }

    public function exec(Command $command)
    {
        $stream = ssh2_exec($this->ssh, (string) $command);
        stream_set_blocking($stream, true);

        $response = stream_get_contents($stream);
        fclose($stream);

        return $response;
    }

    public function __destruct()
    {
        unset($this->ssh);
    }
}
