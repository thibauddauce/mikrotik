<?php

namespace ThibaudDauce\Mikrotik;

class Command
{
    public $command;
    public $parameters;

    public function __construct($command, $parameters = [])
    {
        $this->command = $command;
        $this->parameters = $parameters;
    }

    public function __toString()
    {
        return "/{$this->command} {$this->formatParameters()}";
    }

    protected function formatParameters()
    {
        return collect($this->parameters)
            ->filter()
            ->map(function ($value) {
                if ($value instanceof self) {
                    return "[{$value}]";
                }

                if (str_contains($value, ':')) {
                    return '"' . $value . '"';
                }

                return $value;
            })
            ->map(function ($value, $key) {
                if (is_string($key)) {
                    return "$key=$value";
                }

                return $value;
            })
            ->implode(' ');
    }
}
